#-asdf3.1 (error "ASDF 3.1 or bust!")

(defsystem "fare-hacks"
  :version "0" ; not released
  :description "Random hacks by fare"
  :license "MIT" ; also bugroff
  :author "Francois-Rene Rideau"
  :class :package-inferred-system
  :depends-on ("fare-hacks/hacks"))
