;------>8------>8------>8------>8------>8------>8------>8------>8------>8------
;;; First, a limited CPS implementation using trampolines,
;;; that allows for dynamic binding within continuations.

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defvar *cps-specials* (make-hash-table))
  (defun register-cps-special (x)
    (setf (gethash x *cps-specials*) t)))
(defmacro define-cps-variable (name &rest r)
  `(progn
     (defvar ,name ,@r)
     (eval-when (:compile-toplevel :load-toplevel :execute)
       (register-cps-special ',name))))
(defmacro define-cps-parameter (name initial-value &rest r)
  `(progn
     (defparameter ,name ,initial-value ,@r)
     (eval-when (:compile-toplevel :load-toplevel :execute)
       (register-cps-special ',name))))
(defun cps-specials ()
  (loop :for x :being :the :hash-keys :of *cps-specials* :collect x))
(defmacro rebinding-specials (&body body)
  `(call-rebinding-specials (lambda () ,@body)))
(defun call-rebinding-specials (thunk)
  (let* ((vars (cps-specials))
         (vals (mapcar 'symbol-value vars)))
    (progv vars vals
      (funcall thunk))))

(defun trampoline (c)
  (rebinding-specials
   (loop :for (k . values) = (list c (lambda (&rest values)
                                       (return (apply 'values values))))
     :then (multiple-value-list (apply k values)))))

(defmacro !% (k &rest values)
  (apply 'values k values))
(define-compiler-macro !% (&whole form k &rest args)
  `(values ,k ,@args))
(defmacro ! (&body body)
  (lambda () ,@body))

(defclass dynamic-variable-restore-continuation (funcallable-standard-class)
  ((bindings :initarg :bindings :accessor continuation-bindings)
   (continuation :initarg :continuation :accessor continuation)))

(defmethod initialize-instance :after ((dvrc dynamic-variable-restore-continuation) &key)
  (with-slots (bindings continuation) dvrc
    (set-funcallable-instance-function
     dvrc
     #'(lambda (&rest args)
         (pure:for-each pure:<hash-table> bindings
                        (lambda (var val) (setf (symbol-value var) val)))
         (!% 'apply continuation args)))))

(defun %!dynlet1 (k sym value thunk)
  (let ((previous (symbol-value sym)))
    (setf (symbol-value sym) value)
    (funcall thunk (%!dynrestore sym value k))))

(defun %!dynrestore (sym value k)
  (flet ((mk (bindings continuation)
           (make-instance 'dynamic-variable-restore-continuation
                          :bindings (pure:insert pure:<hash-table> bindings var val)
                          :continuation continuation)))
    (declare (inline mk))
    (typecase k
      (dynamic-variable-restore-continuation
       (with-slots (bindings continuation) k
         (if (nth-value 1 (pure:lookup pure:<hash-table> bindings var))
             k
             (mk bindings continuation))))
      (t
       (mk (pure:empty pure:<hash-table>) k)))))

(defmacro !dynlet1 (k sym value &body body)
  `(%!dynlet1 ,k ,sym ,value (lambda (,k) ,@body)))

