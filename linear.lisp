(eval-when (:compile-toplevel :load-toplevel :execute)
  (require :asdf))
(eval-when (:compile-toplevel :load-toplevel :execute)
  (asdf:load-systems :asdf))
#-asdf3 (error "ASDF 3 required")

(defpackage :linear-lisp-0
  (:use :uiop))

(define-context-free-sexp-grammar trivial-linear-lambda-calculus
  (:parameters (binding-i constants context))
  (:term term
         (or linear-application
             linear-lambda-term
             linear-variable-reference
             constant-reference))
  (:term linear-lambda-term
         (binding-term binding-i context :tag 'λ1 :arity 1 :body term))
  (:term linear-variable-reference
         (bound-variable binding-i context))
  (:term constant-reference
         (bound-variable constants)))

(define-context-free-sexp-grammar trivial-debruijn-linear-lambda-term
  (:parameters (&optional constants)
  (:term ground-term () (term 0))
  (:term term (&optional (depth 0))
         (or (linear-application-term depth)
             (linear-lambda-term depth)
             (cond
               ((= 0 depth) (constant-reference))
               ((= 1 depth) (linear-variable-reference)))))
  (:term linear-application-term (&optional (depth 0))
         (multiple-value-bind (left-vars right-vars)
             (partition-vars depth)
           (sexp (term (length left-vars)) (term (length right-vars)))))
  (:term linear-lambda-term (&optional (depth 0))
         (sexp 'λ1 (term (1+ depth))))
  (:term linear-variable-reference ()
         'var)
  (:term constant-reference
         (bound-variable constants)))
#|

K = dup | cons | ...

L = K | x | L L | λx.L
( dup f x ~= "f x x")

L^{} = K
L^{x} = x
L^V1+V2 = L^V1 L^V2
L^V = λx. L^V+{x}

closed terms: L^{}

C = K | x | C C | λx.C | copy x

C^{},{} = K
C^{x},{x} = x
C^V1+V2,V = C^V1,V C^V2,V
C^{},V+{x} = copy x
C^V,W = λx.C^V+{x},W+{x}

closed terms: C^{},{}

reduction from closed terms of C to closed terms of L: [[ ]]^{}

[[ Kk ]]^{},W = Kk
[[ x ]]^{x},W+{x} = x
[[ x ]]^V, x not in V = FAIL
[[ C1 C2 ]]^V1+V2,W = [[ C1 ]]^V1,W [[ C2 ]]^V2,W
[[ λx.F copy x]]^V,W = dup λx1.x2 [[ λx.F ]]^V,W x1 x2
[[ λx.F]]^V,W, no copy x in F = λx.[[F]]^V+{x},W


Can I have a calculus more like this?

P = K | x | P P | λx.P | persist P | ref P


How do I translate persistent function call?

[[a x y z]]
==> (copy a) x y z

(λ x . x x) (λ x . f x x)



; How do I define the meaning of a function like that?
(def a (x y z)
  (&observe x) (&persistent z) ->
  (cond
    ((list? x) (+ y (length x)))
    ((symbol? x) (+ y (z x)))
    (t y)))

Who is owning x? There are pointers from "under" x to x,
which is kind of allowed in a pseudo-linear system.

The pointers are not allowed to escape.
What is the type of z not to let those pointers escape?



A ::= p ∣ p⊥ ∣ ;; atoms, negation
      A ⊗ A ∣ 1 | ;; positive multiplicatives: times, one.  CL: A⊓A
      A ⅋ A ∣ ⊤ | ;; negative multiplicatives: par, top.    CL: A∧A
      A ⊕ A ∣ 0 ∣ ;; positive additives: plus, zero         CL: A⊔A
      A & A ∣ ⊥ ∣ ;; negative additives: with, bottom       CL: A∨A
      !A ∣ ?A ;; exponentials: of course, why not           CL:

A >– B and A ◦– B
as abbreviations of ∧
| A → B and ◦
| A → B,
⤙⤚
http://www.fileformat.info/info/unicode/category/Sm/list.htm
C-h I TeX
Computability logic: ∧∨⊓⊔ẽ
set: ∩∪ ∅ ∈∋∉⊅  ẽ
multiset:
⊏⊐
⋀⋁⋂⋃∏∐∑∃∀∄
⋄⋅⋆
⊢⊣⊤⊥
⊧⊪
⊦⊨⊩⊫
⊬⊭⊮⊯
↑↓⇓→
↣⇁⇀⇄⇛⇒
°ε

N-ary: ⨁⨂⨅⨆

Typesystems suck that can express !(A⊕B⊸A'⊕B)' but not !((A⊸A')⊗(B⊸B')) or !⨂t:T.(A t⊸A' t)


Government once again successfully defended against the free fox in the free henhouse… you're all invited to the celebratory chicken roast!

11am
Nicole Mathis
315 West 70th Street
Suite 1B
New York, NY 10023
Phone: (212) 280-4740
A ⊸ B := A⊥ ⅋ B ;; lollipop
;; A ⟜
Γ ⊢ Δ

"F⊣G" category theory for F left adjunct to G.
⊨
¬X
x↦y
（）［］｛｝⦅⦆〚〛⦃⦄“”‘’‹›«»「」〈〉《》【】〔〕⦗⦘『』❨❩❪❫❴❵❬❭❮❯❰❱❲❳❛❜❝❞﹙﹚﹛﹜﹝﹞⁽⁾₍₎⦋⦌⦍⦎⦏⦐⁅⁆⸢⸣⸤⸥⟅⟆〈〉⦑⦒⧼⧽〖〗〘〙｢｣⟦⟧⟨⟩⟪⟫⟬⟭⌈⌉⌊⌋⦇⦈⦉⦊﹁﹂﹃﹄︹︺︻︼︗︘︿﹀︽︾﹇﹈︷︸
⏜⏝⎴⎵⏞⏟⏠⏡⟮⟯
λ
𝜆𝛌𝝀𝝺𝞴
ℵ₀


|#;the end
