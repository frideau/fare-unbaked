(in-package :cl-user)

(defun reverse-order (order &key (test 'eql))
  (let ((rev (make-hash-table :test test)))
    (maphash (lambda (node direct-inferiors)
               (dolist (inferior direct-inferiors)
                 (push node (gethash inferior rev nil))))
             table)
    rev))

(defun reverse-index (vector &key (test 'eql) (start 0))
  (let ((indices (make-hash-table :test test)))
    (loop for i from start
          for element in vector do
            (setf (gethash element indices) i))
    indices))

(defun convert-order (nodes order &test 'eql)
  (let* ((nodes (coerce nodes 'vector))
         (size (length nodes))
         (rev (reverse-index nodes :test test))
         (norder (make-array n)))
    (loop for i below n
          for node = (svref nodes i) do
            (setf (svref norder i) (mapcar (lambda (n) (gethash n rev)) (gethash node order))))
    (values size norder nodes rev)))

(defun compute-all-dependencies (nodes order &test 'eql)
  ;; NB: we can map everything to numbers, and compute things faster,
  ;; with all data structures with small O(1) access, instead of hash tables, etc.
  (multiple-value-bind (size back nodes rev) (convert-order nodes order :test test)
    (let ((forward (make-array size :initial-element nil))
          (deps (make-array size :initial-element nil))
          (counts (make-array size :initial-element nil))
          (queue nil)
          (results (make-hash-table :test test)))
      (labels ((enqueue (node)
                 (push node queue))
               (dequeue ()
                 (pop queue))
               (emptydep ()
                 nil)
               (consdeps (elem dep)
                 (cons elem dep))
               (mergedeps (dep1 dep2) ;; Have a faster implementation of integer sets here.
                 (remove-duplicates (append dep1 dep2) :from-end t))) ;; O(n)! FAIL to improve
        ;; Reverse the order table
        (loop for node below size do
          (loop for inferior in (svref back node) do
            (push node (svref forward inferior))))
        ;; Create queue of elements with no back link
        (loop for node below size
              for count = (length (svref back node)) do
              (if (zerop count)
                  (enqueue node)
                  (setf (svref counts node) count)))
        ;; Main loop: propagate dependency list forward
        (loop for node = (dequeue)
              while node do
                (loop with nodedep = (cons node (svref deps node))
                      for dependent in (svref forward node)
                      for depdep = (svref deps dependent) do
                        (setf (svref deps dependent)
                              (merge-deps depdep nodedep))
                        (decf (svref counts dependent))
                        (when (zerop (svref counts dependent))
                          (enqueue dependent))))
        ;; Translate back
        (loop for node below size do
          (setf (gethash (svref nodes node) results)
                (loop for dep in (svref deps node) collect (svref nodes dep))))))))
