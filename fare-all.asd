;;(load "/home/fare/quicklisp/setup.lisp")

(defsystem :fare-all
  :version "0" ; not released
  :description "All packages by fare - for testing"
  :license "MIT" ; also bugroff
  :author "Francois-Rene Rideau"
  ;; NB: asdf-encodings and uiop *BEFORE* poiu
  :defsystem-depends-on (:asdf :asdf-encodings :asdf-driver :uiop :asdf-package-system :load-quicklisp
                               #+(or allegro clisp sbcl single-threaded-ccl) :poiu)
  :depends-on
  (:asdf :asdf-encodings :asdf-driver :uiop :asdf-package-system
   :asdf-finalizers :asdf-system-connections :asdf-dependency-grovel ;; :asdf-flv
   ;;:cl-containers
   :cl-launch :command-line-arguments
   :exscribe :fare-csv :fare-matcher :fare-mop :fare-utils
   :xcvb-driver :xcvb
   :lambda-reader :lambda-reader-8bit
   :meta :nock
   :poiu :ptc :rpm :single-threaded-ccl :wordgames
   ;; :philip-jose
   ;; :quux-time == no .asd
   #+quicklisp :ql-test #+quicklisp :fare-scripts #+quicklisp :workout-timer
   ;; :space-meter
   :fare-todo :fare-hacks ;; :fare-unbaked
   :cl-scripting :fare-scripts
   :asdf-encodings/test
   ;; :asdf-finalizers-test ;; TO DEBUG!
   ;; :cl-containers-test
   :fare-memoization/test
   :inferior-shell :inferior-shell/test
   :lil/test
   :package-renaming-test
   ;; :quux-iolib-test ;; broken for now
   :reader-interception-test
   :scribble/test
   :tthsum-test
   :xcvb-test
   :xcvb-bootstrap :xcvb-bridge))
