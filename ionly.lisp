(uiop:define-package :ionly
  (:use :common-lisp)
  (:use :optima :optima.ppcre :cl-ppcre)
  (:mix :uiop :fare-utils :alexandria))

(in-package :ionly)

(defparameter *file* #p"/home/tunes/Downloads/pes.html")

(defparameter *text* (read-file-string *file*))

(uiop-debug)

(defun extract-text-between (prefix suffix text)
  (nest
   (while-collecting (c))
   (let ((regex (format nil "(?s)^.*?~A(.*?)~A(.*)$"
                        (quote-meta-chars prefix) (quote-meta-chars suffix)))))
   (labels ((rec (text)
              (match text
                ((ppcre regex italic rest)
                 (c italic)
                 (rec rest))))))
   (rec text)))

(defparameter *italic-text* (extract-text-between "<i>" "</i>" *text*))
