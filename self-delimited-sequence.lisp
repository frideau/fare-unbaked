(in-package :cl)


#|
Use interface-passing style as described in fare-utils.

linearity-enforcing wrapper around stateful streams
that ensures that each state is only used once.

pure streams of bits.
stateful streams of bits.

Streams of buffers of bits.
Octets, words, fixnums, bignums, as buffers of bits.
Blitting between buffers.

Have some interface for reading streams.
Have some interface for buffered-streams.
Have some interface for blitting bits into a buffer.
Have some interface for blitting bits into a fixnum.

.... OR ....

|#



;; Allow for theoretically arbitrary long integers, but be efficient for small ones.
;; 1 byte fits 7-bit values (overhead 1 bit), practical for ASCII.
;; "0" 7
;; 2 bytes fit 13-bit values (overhead 3 bits), practical for in-page address, latin characters
;; "1" 7 | "00" 6
;; 2+n bytes fit up to 65528-bit values (overhead 16 bits), for 128ers, crypto.
;; "1" 7 | "01" 6 | read n bytes
;; 3 bytes fit 21-bit values (overhead 3 bits), practical for UNICODE.
;; "1" 7 | "1" 7 | "0" 7
;; 4 bytes fit 27 bit values (overhead 5 bits), practical for most small program addresses.
;; "1" 7 | "1" 7 | "1" 7 | "00" 6
;; 5 bytes fit 36 bit values (overhead 4 bits), pratical for general 32-bit or 36-bit data.
;; "1" 7 | "1" 7 | "1" 7 | "1" 7 | 8
;; 6 bytes fit 40 bit values (overhead 8 bits)
;; "1" 7 | "1" 7 | "1" 7 | "00100" 3 | 8 | 8
;; 7 bytes fit 48 bit values (overhead 8 bits)
;; "1" 7 | "1" 7 | "1" 7 | "00101" 3 | 8 | 8 | 8
;; 8 bytes fit 56 bit values (overhead 8 bits)
;; "1" 7 | "1" 7 | "1" 7 | "00110" 3 | 8 | 8 | 8 | 8
;; 9 bytes fit 64 bit values (overhead 8 bits)
;; "1" 7 | "1" 7 | "1" 7 | "00111" 3 | 8 | 8 | 8 | 8 | 8
;;
;; MAGIC: 128 special markers for other encodings
;; "1" 7 | "000000000"
;; RESERVED:
;; "10000" 3 | "01000000" ;; less-than-8-in-more-than-necessary
;; "10001000" | "01000000" ;; 8-in-more-than-necessary
;; "1" 7 | "10" 6 | "000000000" ;; 2-in-3
;; "1" 7 | "1" 7 | "1" 7 | "000000000" ;; 3-in-4
;; "1" 7 | "1" 7 | "1" 7 | "01" 6 ;; reserved for expansion - 65-bit?
;; "1" 7 | "1" 7 | "1" 7 | "100" 5 | "000000000" ;; 4-in-5
;; "1" 7 | "1" 7 | "1" 7 | "00100" 3 | 8 | "0000" 4 ;; 5-in-6
;; "1" 7 | "1" 7 | "1" 7 | "00101" 3 | 8 | 8 | "00000000" ;; 6-in-7
;; "1" 7 | "1" 7 | "1" 7 | "00110" 3 | 8 | 8 | 8 | "00000000" ;; 7-in-8
;; "1" 7 | "1" 7 | "1" 7 | "00111" 3 | 8 | 8 | 8 | 8 | "00000000" ;; 8-in-9
;; MAGIC value 0:
;; "10000000" | "00000000"
;; recurse to find the length n, then read n bytes — makes it universally applicable.
;; return values: number, number of significant bits read.

(defun sign-extend-byte (x length)
  (let ((i (1- length)))
    (if (logbitp i x)
        (dpb x (byte i 0) -1) ;; (logior (ash -1 i) x)
        x)))
;; without if: (defun sign-extend (x n) (logior x (ash (- (ash x (- 1 n))) n)))

(defmacro decode-bytes-internal (fun reserved read-n-bytes)
  `(block nil
     (let ((accumulator 0)) ;; accumulated low bits of value being read
       (declare (type unsigned-byte accumulator))
       ;; position is assumed statically known by the parser and given
       (macrolet ((done ()
                    `(return accumulator))
                  (accumulate-bits (length position bits)
                    ;; accumulate bits high in front of previous bits
                    `(locally
                         (declare
                          (optimize (speed 3) (safety 0) (size 1))
                          (type (unsigned-byte ,(+ position length)) accumulator))
                       (setf (ldb (byte ,length ,position) (accumulator) ,bits))))
                  (done-length (bytes-read minimum)
                    ;; The accumulator is the length of bytes to read
                    ;; These must include at least the minimum bytes.
                    `(if (< accumulator ,minimum)
                         (funcall reserved ,(- minimum) accumulator)
                         (done-verify-bytes (+ 2 accumulator)
                                            (funcall read-n-bytes accumulator)
                                            accumulator)))
                  (done-verify (bytes-read value length previous)
                    (if (zerop (ldb (byte (- length previous) previous)))
                        (funcall reserved bytes-read previous value)
                        (done)))
               (done-verify-bytes (value length)
                 (let* ((bit-length (* 8 length))
                        (previous (- bit-length 8)))
                   (done-verify value bit-length previous)))
               (done-verify-accumulator (length previous)
                 (done-verify accumulator position previous))
               (accumulate-bytes (n)
                 (accumulate-bits (* 8 n) (funcall read-n-bytes n)))
               (last-n-bytes (n previous)
                 (accumulate-bytes n)
                 (done-verify previous)))
        (funcall fun :reserved reserved :read-n-bytes read-n-bytes
                 :accumulate-bits #'accumulate-bits
                 :done #'done :done-verify-accumulator #'done-verify-accumulator
                 :done-length #'done-length
                 :last-n-bytes #'last-n-bytes)))))

(defmacro decode-bytes ((&rest context) op)
  (let ((previous-done-bits 0))
    (labels
        ((parse-op (x bytes-read bits-accumulated)
           (ematch x
             (`(! ,total-bytes ,total-bits ,previous)
              ;; Just return the accumulated result.
              (assert (= bytes-read total-bytes))
              (assert (= bits-accumulated total-bits))
              (assert (= previous previous-done-bits))
              (assert (> total-bits previous))
              (setf previous-done-bits total-bits)
              (if (zerop previous)
                  `(done)
                  `(done-verify-accumulator length previous)))
             (`(? ,previous ,bits)
              ;; Reserved value.
              `(funcall reserved ,bytes-read ,previous ,bits))
             (`(!length ,total-bytes ,total-bits ,minimum)
              (assert (= bytes-read total-bytes))
              (assert (= bits-accumulated total-bits))
               `(done-length ,bytes-read ,minimum))
             (`(!! ,bytes-left ,total-bytes ,total-bits ,previous)
              (assert (= (+ bytes-read bytes-left) total-bytes))
              (assert (= (+ bits-accumulated (* 8 bytes-left)) total-bits))
              (assert (= previous previous-done-bits))
              (setf previous-done-bits total-bits)
               `(last-n-bytes ,bytes-left ,total-bytes ,total-bits ,previous))
             (`(get ,@clauses)
              `(let ((byte (read-byte)))
                 ,(parse-get 0 8 (parse-clauses clauses) (1+ bytes-read) bits-accumulated)))))
         (parse-clauses (clauses)
           (loop :for (prefix-string bits) :in clauses :do
             (check-type prefix-string string)
             (assert (= 8 (+ bits (length string))))
             :collect (list (ash (parse-integer prefix-string :radix 2) bits) bits)))
         (parse-get (prefix undecoded-bits clauses bytes-read bits-accumulated)
           (cond
             ((null clauses)
              (error "unhandled decoding case ~S" (list prefix undecoded-bits bytes-read bits-accumulated)))
             ((null (cdr clauses))
              (destructuring-bind ((clause-prefix clause-bits)) clauses
              (assert (= clause-prefix prefix))
              (assert (= undecoded-bits clause-bits))
              `(

           (let* ((most-bits-wanted (loop :for (prefix bits) :in clauses :maximize bits))
                  (bits-to-decode (- undecoded-bits most-bits-wanted)))
             (assert (plusp bits-to-decode))
             `(let ((discriminator (ldb (byte ,bits-to-decode ,most-bits-wanted) byte)))
                (case discriminator
                  ,@(bucket-clauses
                     bits-to-decode most-bits-wanted clauses
                     bytes-read bits-accumulated)))))
         (bucket-clauses (len pos clauses bytes-read bits-accumulated)
           (loop :with ncases = (ash 1 len)
             :with buckets = (make-array (list ncases) :initial-element nil)
             :with redro = ()
             :for clause :in clauses
             :for value = (ldb (byte len pos) (first clause)) :do
             (when (null (aref buckets value))
               (push value redro))
             (push clause (aref buckets value))
             :finally
             (assert (= ncases (length redro)))
             (return (loop :for value :in (reverse redro) :collect
                       (handle-get-subclauses pos (aref buckets value)
                                              bytes-read bits-accumulated))



           ((cons 'get clauses)

         xxx)
       xxx)
    `(decode-bytes-runtime
      (lambda (&key reserved read-n-bytes accumulate-bits
               done done-verify done-length last-n-bytes)
        ,(parse-op op)
        ,reserved ,read-n-bytes))))

(define-byte-matcher
  (decode-bytes
    (get
     (("0" 7) (! 1 7 0)) ; 1-byte 7-bit number, over a hundred, encodes ASCII
     (("1" 7)
      (get
       (("00" 6) (! 2 13 7)) ; 2-byte 13-bit number, over a thousand
       (("01" 6) (!length 2 13 9)) ; 2-byte 13-bit length, must be more than 9
       (("1" 7)
        (get
         (("0" 7) (! 3 21 13)) ; 3-byte 21-bit number, over a million, encodes UNICODE
         (("1" 7)
          (get
           (("00" 6) (! 4 27)) ; 4-byte 27-bit number, over ten million
           (("1" 7) (!! 1 5 36 27)))) ; 5-byte 36-bit number, over ten billion
           (("01000" 3) (!! 2 6 40 36)) ; 6-byte 40-bit number, over a trillion
           (("01001" 3) (!! 3 7 48 40)) ; 7-byte 48-bit number, over a hundred trillion
           (("01010" 3) (!! 4 8 56 48)) ; 8-byte 56-bit number, over a ten quadrillion
           (("01011" 3) (!! 5 9 64 56)) ; 9-byte 64-bit number, over a ten quintillion
           (("011" 5) (? 4 26)))))))))


(define-byte-matcher
  (decode-bytes
    (get
     (("0" 7) (!! 1 7 0)) ; 1-byte 7-bit number, over a hundred, encodes ASCII
     (("100" 5) (!! 2 13 7)) ; 2-byte 13-bit
     (("101" 5) (!!length 2 13 9)) ; 2-byte 13-bit length
     (("110" 5) (!! 3 21 13)) ; 3-byte 21-bit
     (("111110" 2) (!! 4 26 21)) ; 4-byte 26-bit
     (("111111" 2) (? 1 2)) ; MAGIC values
     (("1110" 4) (!! 5 36 26)) ; 5-byte 36-bit
     (("11110100" 0) (!! 6 40 36)) ; 6-byte 40-bit
     (("11110101" 0) (!! 7 48 40)) ; 7-byte 48-bit
     (("11110110" 0) (!! 8 56 48)) ; 8-byte 56-bit
     (("11110111" 0) (!! 9 64 56)) ; 9-byte 64-bit

(define-byte-matcher
  (decode-bytes
    (get
     (("0" 7) (!! 1 7 0)) ; 1-byte 7-bit number, over a hundred, encodes ASCII
     (("100" 5) (!! 2 13 7)) ; 2-byte 13-bit
     (("101" 5) (!! 3 21 13)) ; 3-byte 21-bit
     (("110" 5) ;; 4 bytes of length + one bit of sign?
     (("111110" 2) (!! 4 26 21)) ; 4-byte 26-bit
     (("111111" 2) (? 1 2)) ; MAGIC values
     (("1110" 4) (!! 5 36 26)) ; 5-byte 36-bit
     (("11110100" 0) (!! 6 40 36)) ; 6-byte 40-bit
     (("11110101" 0) (!! 7 48 40)) ; 7-byte 48-bit
     (("11110110" 0) (!! 8 56 48)) ; 8-byte 56-bit
     (("11110111" 0) (!! 9 64 56)) ; 9-byte 64-bit

;; one-instruction ffs scans will do the job (modulo negation).
(define-byte-matcher
  (decode-bytes
     (("0" 7) (!! 1 7 0)) ; 1-byte 7-bit number, over a hundred, encodes ASCII
     (("10" 6) (!! 2 14 7)) ; 2-byte 14-bit
     (("110" 5) (!! 3 21 14)) ; 3-byte 21-bit
     (("1110" 4) (!! 4 28 21)) ; 4-byte 26-bit ;; sacrifice these numbers for the sake of
     (("11110" 3) (!! 5 36 28)) ; 5-byte 36-bit ;; make it easier for 36-biters?
     (("111110" 2) (!! 6 42 36)) ; 6-byte 42-bit
     (("1111110" 1) (!! 7 49 42)) ; 7-byte 49-bit
     (("11111110" 0) (!! 8 56 49)) ; 8-byte 56-bit
     (("11111111" 0) (!! 9 64 56)) ; 9-byte 64-bit
;; for numbers from 9 to 125 bytes, use the denormal value "10" 6 | "0000000" 1
;; with n being the number of bytes to load. values 0 to 8 and 126 to 127 are reserved.
;; the first denormal value means "recurse": "10000000" | "00000000" | length, then number
;; NB: information density drops between 64-bit and 128-bit data, then gets very good.
;; for numbers from

;; In terms of bytes, the overhead is less than one additional byte
;; for all values under 16 bytes.
;; It's less than 2 additional bytes for all values under 1016 bits,
;; less than 3 additional bytes for all values under 131064 bits,
;; less than 4 additional bytes for all values under 16777208 bits,
;; less than 5 additional bytes for all values under 268435448 bits,
;; less than 6 additional bytes for all values under 549755813880 bits, etc.
;; There is no need for double recursion on conceivable computers.
;; values returned by the read function: number, bit length read, fount
(define-byte-matcher
  (decode-bytes
     (("0" 7) (!! 1 7 0)) ; 1-byte 7-bit number, over a hundred, encodes ASCII
     (("10" 6) (!! 2 14 7)) ; 2-byte 14-bit
     (("110" 5) (!! 3 21 14)) ; 3-byte 21-bit
     (("11110000" 0) (!recurse 128)) ; recurse: read length, then read bytes.
     (("11110001" 0) (reserved)) ; reserved
     (("1111001" 1) (!! 4 25 21)) ; 4-byte 25-bit ;; sacrifice these numbers for the sake of
     (("1110" 4) (!! 5 36 25)) ; 5-byte 36-bit ;; make it easier for 36-biters?
     (("11110100" 0) (!! 6 40 36)) ; 6-byte 40-bit
     (("11110101" 0) (!! 7 48 40)) ; 7-byte 48-bit
     (("11110110" 0) (!! 8 56 48)) ; 8-byte 56-bit
     (("11110111" 0) (!! 9 64 56)) ; 9-byte 64-bit
     (("11111000" 0) (!! 10 72 64)) ; 10-byte 72-bit
     (("11111001" 0) (!! 11 80 72)) ; 11-byte 80-bit
     (("11111010" 0) (!! 12 88 80)) ; 12-byte 88-bit
     (("11111011" 0) (!! 13 96 88)) ; 13-byte 96-bit
     (("11111000" 0) (!! 14 104 96)) ; 14-byte 104-bit
     (("11111001" 0) (!! 15 112 104)) ; 15-byte 112-bit
     (("11111010" 0) (!! 16 120 112)) ; 16-byte 120-bit
     (("11111111" 0) (!! 17 128 120)))) ; 17-byte 128-bit
;; for numbers from 129 bits to 1024 bits, i.e. 17-byte to 128-byte,
;; with n being the number of bytes to load minus 1. values n 0 to 15 are reserved.
;; value 0 means "recurse": "10000000" | "00000000" | length, then number
;; NB: information density in *bits* drops 27-bit and 63-bit, then gets very good.
;; In bytes, it's great.

(defun decode-self-delimited-bytes (fount-interface fount)
  (with-decoding (xxx)
    (multiple-value-bind (byte fount) (read-byte fount-interface fount)
      (cond
        (("0" 7) (!! 0 7 0))
        (("10" 6) (!! 1 14 7))
        (("110" 5) (!! 2 21 14))
        (("1110" 4) (!! 4 36 26))
        (("111100" 2) (!! 3 26 21))
        (("11110100" 0) (!! 5 40 36))
        (t (let ((n (1+ (logand byte 15)))) (!!** n)))))))


(define-byte-matcher
  (decode-bytes
     (("0" 7) (!! 1 7 0)) ; 1-byte 7-bit number, over a hundred, encodes ASCII
     (("100" 5) (!! 2 13 7)) ; 2-byte 13-bit
     (("101" 5) (!! 3 21 13)) ; 3-byte 21-bit
     (("1100" 4) (!! 4 28 21)) ; 4-byte 28-bit
     (("1101" 4) (!! 5 36 28)) ; 5-byte 36-bit ;; make it easier for 36-biters?
     (("11100000" 0) (!! 6 40 36)) ; 6-byte 40-bit
     (("11100001" 0) (!! 7 48 40)) ; 7-byte 48-bit
     (("11100010" 0) (!! 8 56 48)) ; 8-byte 56-bit
     (("11100011" 0) (!! 9 64 56)) ; 9-byte 64-bit
     (("11100100" 0) (!! 10 72 64)) ; 10-byte 72-bit
     (("11100101" 0) (!! 11 80 72)) ; 11-byte 80-bit
     (("11100110" 0) (!! 12 88 80)) ; 12-byte 88-bit
     (("11100111" 0) (!! 13 96 88)) ; 13-byte 96-bit
     (("11101000" 0) (!! 14 104 96)) ; 14-byte 104-bit
     (("11101001" 0) (!! 15 112 104)) ; 15-byte 112-bit
     (("11101010" 0) (!! 16 120 112)) ; 16-byte 120-bit
     (("11101011" 0) (!! 17 128 120)) ; 17-byte 128-bit
     (("111011" 2) (reserved))
     (("1111" 4 (reserved)))))


;;;; If you have dealing with bits rather than bytes, then
;;;; this encoding is extremely efficient.
;;;; This is a variant of Elias Omega Coding.
(defun decode-self-delimited-bits (fount-interface fount)
  (multiple-value-bind (bit fount) (read-bit fount-interface fount)
    (if (zerop bit)
        (values 0 fount)
        (multiple-value-bind (length fount)
            (decode-self-delimited-bits fount-interface fount)
          (multiple-value-bind (bits fount)
              (decode-fixed-bits fount-interface fount length)
            (values (+ (ash 1 length) bits) fount))))))

(defun encode-self-delimited-bits (sink-interface sink n)
  (if (zerop n)
      (write-bit sink-interface sink 0)
      (let* ((sink1 (write-bit sink-interface sink 1))
             (length (1- (integer-length n)))
             (sink2 (encode-self-delimited-bits sink-interface sink1 length)))
        (encode-fixed-bits sink-interface sink2 length n))))

(test-bit-encoding #'encode-self-delimited-bits #'decode-self-delimited-bits
 #*0 0
 #*10 1
 #*1100 2
 #*1101 3
 #*1110000 4
 #*1110001 5
 #*1110010 6
 #*1110011 7
 #*11101000 8
 #*11101001 9
 #*11101010 10
 #*11101011 11
 #*11101100 12
 #*11101101 13
 #*11101110 14
 #*11101111 15
 #*111100000000 16
 #*111100001111 31
 #*1111000111010 42
 #*1111000111111 63
 #*11110010111111 127
 #*111100110101010 #xAA
 #*111100111111111 255
 #*111101111111000000001101 #xF00D
 #*11111000011111011110101011011011111011101111 #xDEADBEEF)


(defpackage :self-delimited-sequence (:use :cl :fare-utils :interface :fmemo))

(in-package :self-delimited-sequence)

(defclass <fount> (<interface>) ())
(defgeneric input-item (<fount> fount))

(defclass <sink> (<interface>) ())
(defgeneric output-item (<sink> sink item))

(defclass <octet-fount> (<fount>) ())
(defclass <octet-sink> (<sink>) ())

(defclass <unmarshalling> (<octet-sink>) ())
(defgeneric decode-immediate-object (<unmarshalling> octet))
(define-condition object-too-big (error) ())
(define-condition high-octet-cant-be-zero (parse-error) ())

(defclass <marshalling> (<octet-fount>) ())

(defgeneric allocate-buffer (<interface> size))
(defgeneric finalize-buffer (<interface> buffer))

(defclass <list-fount> (<fount>) ())
(defclass <vector-fount> (<fount>) ())

(defclass <impure-byte-input-stream> (<octet-fount>) ())
(defmethod input-item ((i <impure-byte-input-stream>) stream)
  (values (read-byte stream) stream))

(defclass <impure-byte-output-stream> (<octet-sink>) ())
(defmethod output-item ((i <impure-byte-output-stream>) stream byte)
  (write-byte byte stream)
  stream)

(defclass <vector-fount> (<fount>) ()) ;; pure
(define-memo-function <vector-fount> ()
  (make-instance '<vector-fount>))
(defmethod make-vector-fount (vector)
  (check-type vector vector)
  (cons vector 0))
(defmethod input-item ((i <vector-fount>) buffer)
  (destructuring-bind (vector . pointer) buffer
    (let ((value (aref vector pointer)))
      (values value (cons vector (1+ pointer))))))

(defclass <impure-byte-output-stream> (<octet-sink>) ())
(defmethod output-item ((i <impure-byte-output-stream>) stream byte)
  (write-byte byte stream)
  stream)


(defclass <unsigned-integer-unmarshalling> (<unmarshalling>) ())
(define-memo-function <unsigned-integer-unmarshalling> ()
  (make-instance '<unsigned-integer-unmarshalling>))
(defmethod allocate-buffer ((i <unsigned-integer-unmarshalling>) size)
  (unless (< size most-positive-fixnum)
    (error 'object-too-big))
  (vector 0 0 size)) ; little-endian number so far, position, total size.
(defmethod finalize-buffer ((i <unsigned-integer-unmarshalling>) buffer)
  (vector-bind (value number-of-input-octets size) buffer
    (assert (= number-of-input-octets size))
    value))
(defmethod output-item ((i <unsigned-integer-unmarshalling>) buffer octet)
  (check-type buffer (simple-vector 3))
  (check-type octet (unsigned-byte 8))
  (vector-bind (so-far fill-pointer size) buffer
    (let ((new-fill-pointer (1+ fill-pointer)))
      (when (= new-fill-pointer size)
        (when (zerop octet)
          (error 'high-octet-cant-be-zero)))
      (vector (dpb octet (byte 8 (* 8 fill-pointer)) so-far)
              new-fill-pointer size))))
(defmethod decode-immediate-object ((i <unsigned-integer-unmarshalling>) value)
  value)


(deftype octet () '(unsigned-byte 8))
(deftype simple-octet-vector (length-or-*)
  `(vector (octet) ,length-or-*))
(defun octet-vector (&rest octets)
  (make-array (length octets) :initial-contents octets))

(defclass <simple-octet-vector-unmarshalling> (<vector-unmarshalling>) ())
(defgeneric element-type (interface))

(defclass <simple-octet-vector-unmarshalling> (<vector-unmarshalling>) ())
(define-memo-function <simple-octet-vector-unmarshalling> ()
  (make-instance '<simple-octet-vector-unmarshalling>))
(defmethod allocate-buffer ((i <simple-octet-vector-unmarshalling>) size)
  (unless (< size most-positive-fixnum)
    (error 'object-too-big))
  (vector (make-array size :element-type '(unsigned-byte 8)) 0))
(defmethod finalize-buffer ((i <simple-octet-vector-unmarshalling>) buffer)
  (vector-bind (vector fill-pointer) buffer
    (assert (= (length vector) fill-pointer))
    vector))
(defmethod output-item ((i <simple-octet-vector-unmarshalling>) buffer octet)
  (check-type buffer (simple-vector 2))
  (check-type octet (octet))
  (vector-bind (vector fill-pointer) buffer
    (setf (aref vector fill-pointer) octet)
    (incf fill-pointer)
    (when (= fill-pointer (length vector))
      (when (zerop octet)
        (error 'high-octet-cant-be-zero))))
  buffer)
(defmethod decode-immediate-object ((i <simple-octet-vector-unmarshalling>) value)
  (make-array 1 :element-type 'octet :initial-element value))

(defun signed-integer-from-self-delimited-unsigned-integer (u)
  (let ((val (ash u -1)))
    (if (oddp u)
        (1+ val)
        (- val))))

(defun self-delimited-unsigned-integer-from-signed-integer (i)
  (if (plusp i)
      (1+ (ash i 1))
      (ash (- i) 1)))

(defgeneric repeat-fount-to-sink (<fount> <sink> fount sink repeat))

(defmethod repeat-fount-to-sink ((<fount> <fount>)
                                 (<sink> <sink>)
                                 fount sink repeat)
  (loop :with item :repeat repeat :do
    (multiple-value-setq (item fount) (input-item <fount> fount))
    (setf sink (output-item <sink> sink item)))
  (values fount sink))

(defgeneric read-fixed-length-sequence (<fount> <sink> fount length &key))

(defmethod read-fixed-length-sequence ((<fount> <fount>)
                                       (<sink> <sink>)
                                       fount length
                                       &key (max-length most-positive-fixnum))
  (unless (< length max-length)
    (error 'object-too-big))
  (let ((sink (allocate-buffer <sink> length)))
    (multiple-value-bind (fount sink)
        (repeat-fount-to-sink <fount> <sink> fount sink length)
      (values (finalize-buffer <sink> sink) fount))))

(defgeneric read-self-delimited-unsigned-integer (<fount> fount &key))

(defmethod read-self-delimited-unsigned-integer
    ((<fount> <fount>) fount
     &key (current-length 1) (byte-size 8)
     (max-length most-positive-fixnum))
  (unless (< (* byte-size (1- current-length)) (integer-length max-length))
    (error 'object-too-big))
  (unless (<= 4 byte-size)
    (error "byte-size ~D too small" byte-size))
  (let ((<uiu> (<unsigned-integer-unmarshalling>)))
    (multiple-value-bind (value fount)
        (read-fixed-length-sequence <fount> <uiu> fount current-length)
      (let ((bitpos (1- (* byte-size current-length))))
        (DBG :rsdui value bitpos (ldb (byte 1 bitpos) value))
        (if (zerop (ldb (byte 1 bitpos) value))
            (values value fount)
            (let ((next-length (ldb (byte (1- bitpos) 0) value))
                  (recurse-further-p (ldb (byte 1 (1- bitpos)) value)))
              (DBG :blah next-length recurse-further-p)
              (if (plusp recurse-further-p)
                  (read-self-delimited-unsigned-integer
                   <fount> fount :current-length next-length
                   :max-length (ceiling (integer-length max-length) byte-size))
                  (read-fixed-length-sequence <fount> <uiu> fount next-length))))))))


#|
(defmacro pure-to-classy (xxx)
  xxx)


(defmacro classy-to-pure (xxx)
  xxx)


(define-interface pure-interface (xxx)
  xxx)

(defmethod pure-method ((i pure-interface) state &rest arguments)
  ...
  (values new-state . results))

(define-class impure-class (xxx)
  xxx)

(defmethod classy-method ((x impure-class) &rest arguments)
  xxx
  (values . results))


(defclass classified-interface (<interface> simple-value-box)
  ((state :accessor classified-interface-state :initarg :state)))

(defmacro define-classified-methods ((object-var) classy-pure-method-specifiers)
  `(progn
     ,@(dolist (spec classy-pure-method-specifiers)
         (destructuring-bind (&key original-name transformed-name
                                   argument-list return-list)
             (parse-transformed-function-signature spec)
           ;; TODO: either preparse argument list into mandatory, optional, etc., or
           ;; at least check that we're not getting an &keyword
           (destructuring-bind (interface-argument state-argument &rest more-arguments)
               argument-list

           (let ((interface-state-arguments
                  `(
                 (transformed-argument-list
                  `(,object-var ,(cddr argument-list ...)))
           `(defmethod ,transformed-name (,object-var


|#
