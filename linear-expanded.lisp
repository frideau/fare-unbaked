(define-interface trivial-linear-lambda-calculus (context-free-sexp-grammar)
  ((binding-i)
   (constants)
   (context))
  (:generic term<-linear-application (xxx) yyy)
  (:generic (term<-linear-application (xxx) yyy)
  (:generic (term<-linear-lambda-term (xxx) yyy)
  (:generic (term<-linear-variable-reference (xxx) yyy)
  (:generic (term<-constant-reference (xxx) yyy)



  (:parameters (binding-i constants context))
  (:term term
         (or linear-application
             linear-lambda-term
             linear-variable-reference
             constant-reference))
  (:term linear-lambda-term
         (binding-term binding-i context :tag 'λ1 :arity 1 :body term))
  (:term linear-variable-reference
         (bound-variable binding-i context))
  (:term constant-reference
         (bound-variable constants)))
